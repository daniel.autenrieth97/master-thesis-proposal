from ogb.linkproppred import PygLinkPropPredDataset
import torch
import numpy as np
from ogb.linkproppred import Evaluator
d_name = "ogbl-collab"



def create_data_files():
    # load dataset (first time running will download zip file otherwise existing file in dataset folder will be used)
    dataset = PygLinkPropPredDataset(name = d_name)
    # split loaded data
    split_edge = dataset.get_edge_split()
    train_edge, valid_edge, test_edge = split_edge["train"], split_edge["valid"], split_edge["test"]
    print(f"Size normal: {len(test_edge['edge'])}, Size Neg: {len(test_edge['edge_neg'])}")
    
    # Get list of 128-bit edge embeddings (see documentation)
    edge_emb = dataset[0]['x']
    # Call rest of logic for different filenames & data
    create_one_file(train_edge, edge_emb, 'train')
    create_one_file(valid_edge, edge_emb, 'valid')
    create_one_file(test_edge, edge_emb, 'test')
    
    return

def write_file(data ,name):
    with open(f'{name}.libfm', 'w') as f:
        for line in data:
            f.write(line)
            f.write('\n')
    print(f'Generated {name}.libfm')
    return

def create_one_file(split_set, edge_emb, name):
    data_lines = data_structure(split_set=split_set, edge_emb=edge_emb, edge_name='edge')
    # Append negative examples if possible
    if 'edge_neg' in split_set.keys():
        data_lines.extend(data_structure(split_set=split_set, edge_emb=edge_emb, edge_name='edge_neg'))
    write_file(data_lines, name)
    return

def data_structure(split_set, edge_emb, edge_name):
    # Fill list with data
    data_lines= []
    # Iterate over all edges
    for index, edge in enumerate(split_set[edge_name]):
        # edge = [node1, node2]
        # Add Weight at the beginning
        if edge_name == 'edge_neg':
            line = f"{0} "
        else:
            line = f"{split_set['weight'][index]} "
        # Embeddings node 1
        for index_node1, val in enumerate(edge_emb[edge[0]-1]):
            if val != 0.0:
                line += f"{index_node1}:{val} "
        # Embeddings node 2
        for index_node2, val in enumerate(edge_emb[edge[1]-1]):
            if val != 0.0:
                line += f"{index_node1+index_node2+1}:{val} "
        #Add year at the end
        # try:
        #     line += f"{index_node1+index_node2+2}:{split_set['year'][index]} "
        # except:
        #     line += f"{index_node1+index_node2+2}:2000 "
        #Append line
        data_lines.append(line)

        if index >= 200000:
            break

    print(f'{index} lines written')
    return data_lines

def load_file(filename):
    file1 = open(filename, 'r')
    Lines = file1.readlines()
    Lines = [s.strip('\n') for s in Lines]
    return Lines

def evaluate():
    y_pred = load_file('C:\\Users\\danie\\Desktop\\Master Thesis\\libfm\\test.pred')
    evaluator = Evaluator(name = d_name)
    print(evaluator.expected_input_format) 
    print(evaluator.expected_output_format)

    half = int(len(y_pred)/2)
    y_pred_pos = np.float_(y_pred[:half])
    y_pred_neg = np.float_(y_pred[half:])

    # In most cases, input_dict is
    input_dict = {'y_pred_pos': y_pred_pos, 'y_pred_neg': y_pred_neg}
    result_dict = evaluator.eval(input_dict)
    print(result_dict)
    return

def main():
    print('Creating data files which can be found in the Python directory.')
    j = input('Do you want to create data files? Input y for yes or any other key to skip this step:\n')
    if j == "y" or j == "Y":
        create_data_files()
        print('Files were created. Please use those files to train your model. Afterwards continue this programm.')
    x = input('Input x to terminate the programm or input any other key to start the Evaluation:\n')
    if not x =='x' and not x == 'X':
        evaluate()
    print('Programm finished')
    return

if __name__ == "__main__":
    main()

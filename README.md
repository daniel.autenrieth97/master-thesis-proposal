# Master Thesis Proposal

This repository contains the first draft of the programming part for the master thesis of Daniel Autenrieth. The files located here serve as a proof of concept for the feasibility of the project. 

## Installation

To interpret the included Python code, the OGB library is required. Follow the instructions on the official homepage:<br />
https://ogb.stanford.edu/docs/home/

To follow the complete process libFM is necessary. The software can be easily cloned from the following repository:<br />
https://github.com/srendle/libfm

Use the following folder structure:

    ├── MasterThesisProposal
    │  ├── libfm
    │  │  ├── libfm.exe
    │  │  ├── Generated data files (move here after python execution)
    │  │  ├── Prediction file (after libfm execution)
    │  ├── Python
    │  │  ├── DataPrePro & Evaluation


## Usage

Start the downloaded Python file and follow the instructions. The first time you run it, the dataset is downloaded and a folder is created for it. After creating the data files you have to run libFM. To do this, copy your generated data into the libFM folder. A more detailed description of how to start the program and how it works can be found in the ReadMe of the libFM repository. The program will then generate a prediction file based on your files. After this file appears in the libFM folder you can run the Python file and look at the evaluation metrics.

>At the moment the number of examples are limited. If you want to change the value, do it in the following line:
```python
if index >= 200000:
    break
```


